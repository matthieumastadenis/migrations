<?php

namespace ThibaudDauce\Migrations\Relations;

use Illuminate\Database\Schema\Blueprint;

class BelongsTo
{
    public function __invoke($name, Blueprint $table)
    {
        $relation = $table->getModel()->$name();
        $keyType = $this->getKeyType($relation);

        $table->foreign($relation->getForeignKeyName())->references($relation->getOwnerKeyName())->on($relation->getRelated()->getTable());
        return $table->$keyType($relation->getForeignKeyName())->unsigned();
    }

    public function getKeyType($relation)
    {
        if ($relation->getRelated()->getKeyType() === 'string') {
            return 'string';
        }

        return 'integer';
    }
}
