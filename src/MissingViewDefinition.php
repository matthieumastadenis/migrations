<?php declare(strict_types=1);

namespace ThibaudDauce\Migrations;

class MissingViewDefinition extends MigrationException
{
    public function __construct(ViewMigration $migration)
    {
        $modelMethod = get_class($migration->getModel()) . "@{$migration->getVia()}";
        $migrationMethod = get_class($migration) . "@{$migration->getVia()}";

        parent::__construct(
            "The view definition should exists in `$modelMethod` or in `$migrationMethod`. Neither method is defined."
        );
    }
}