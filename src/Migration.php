<?php

namespace ThibaudDauce\Migrations;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration as LaravelMigration;

class Migration extends LaravelMigration
{
    use InteractsWithModel;

    protected $via = 'schema';

    public function up()
    {
        $model = $this->getModel();

        if (method_exists($model, 'before_schema_migration')) {
            $model->before_schema_migration();
        }

        Schema::connection($model->getConnectionName())->create($model->getTable(), function (Blueprint $table) use ($model) {
            $table->setModel($model);
            $this->applySchema($table);
        });

        if (method_exists($model, 'after_schema_migration')) {
            $model->after_schema_migration();
        }
    }

    public function down()
    {
        $model = $this->getModel();
        Schema::connection($model->getConnectionName())->dropIfExists($model->getTable());
    }

    protected function applySchema(Blueprint $table)
    {
        if (method_exists($this, $this->getVia())) {
            return $this->{$this->getVia()}($table);
        }

        if (method_exists($this->getModel(), $this->getVia())) {
            return $this->getModel()->{$this->getVia()}($table);
        }

        throw new MissingTableDefinition($this);
    }

    public function getVia()
    {
        return $this->via;
    }
}
