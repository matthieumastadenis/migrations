<?php declare(strict_types=1);

namespace ThibaudDauce\Migrations;

class MissingModelAttributeException extends MigrationException
{
    public function __construct()
    {
        parent::__construct("The `model` attribute must be set on the migration class.");
    }
}