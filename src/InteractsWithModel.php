<?php declare(strict_types=1);

namespace ThibaudDauce\Migrations;

use Illuminate\Database\Eloquent\Model;

trait InteractsWithModel
{
    protected $model;

    public function getModel(): Model
    {
        if (is_null($this->model)) {
            throw new MissingModelAttributeException;
        }

        return new $this->model;
    }
}