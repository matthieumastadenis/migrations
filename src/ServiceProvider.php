<?php

namespace ThibaudDauce\Migrations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    public function boot()
    {
        Blueprint::macro('setModel', function (Model $model) {
            $this->model = $model;
        });
        Blueprint::macro('getModel', function () {
            if (! $this->model) {
                throw new UnsetModel;
            }

            return $this->model;
        });

        Blueprint::macro('relation', function ($name) {
            $relation = class_basename($this->getModel()->$name());

            $class = "ThibaudDauce\\Migrations\\Relations\\{$relation}";
            return (new $class)($name, $this);
        });

        $this->commands([
            RefreshViewCommand::class,
        ]);
    }
}
