<?php declare(strict_types=1);

namespace ThibaudDauce\Migrations\Stubs;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $guarded = [];
}