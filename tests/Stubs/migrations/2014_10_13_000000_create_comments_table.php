<?php

use ThibaudDauce\Migrations\Migration;
use ThibaudDauce\Migrations\Stubs\Comment;

class CreateCommentsTable extends Migration
{
    protected $model = Comment::class;
}
