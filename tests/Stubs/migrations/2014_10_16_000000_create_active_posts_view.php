<?php

use ThibaudDauce\Migrations\Stubs\ActivePost;
use ThibaudDauce\Migrations\Stubs\Post;
use ThibaudDauce\Migrations\ViewMigration;

class CreateActivePostsView extends ViewMigration
{
    protected $model = ActivePost::class;

    public function schema()
    {
        return Post::whereRaw('active', true);
    }
}
