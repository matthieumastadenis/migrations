<?php

use ThibaudDauce\Migrations\Stubs\Search;
use ThibaudDauce\Migrations\ViewMigration;

class CreateSearchesView extends ViewMigration
{
    protected $model = Search::class;
}
