<?php

namespace ThibaudDauce\Migrations\Stubs;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $guarded = [];

    protected $casts = [
        'searchable_id' => 'int',
    ];

    static $schema = null;

    public function schema()
    {
        if (self::$schema) {
            return self::$schema;
        }

        return self::setDefaultSchema();
    }

    public static function setDefaultSchema()
    {
        $titles = DB::table('posts')->select('title as term', 'id as searchable_id', Post::class . ' as searchable_type');
        $bodies = DB::table('posts')->select('body as term', 'id as searchable_id', Post::class . ' as searchable_type');
        $comments = DB::table('comments')->select('body as term', 'id as searchable_id', Comment::class . ' as searchable_type');

        return self::$schema = $titles->union($bodies)->union($comments);
    }

    public static function setOnlyTitleSchema()
    {
        self::$schema = DB::table('posts')->select('title as term', 'id as searchable_id', Post::class . ' as searchable_type');
    }
}
