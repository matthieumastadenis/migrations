<?php

namespace ThibaudDauce\Migrations\Stubs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;

class StringIDModel extends Model
{
    public $incrementing = false;
    protected $table = 'string_id_models';
    protected $keyType = 'string';
    protected $guarded = [];

    public function schema(Blueprint $table)
    {
        $table->string('id')->unique();
        $table->timestamps();
    }
}
